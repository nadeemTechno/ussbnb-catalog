<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

$router->group(['prefix' => '/api/v1'], function () use ($router) {

    /** Countries */

    $router->get('/countries', array('as'=>'Country List', 'uses'=>'CountryController@GetCountryList'));
    $router->post('/countries', array('as'=>'Create Country', 'uses'=>'CountryController@CreateCountry'));
    $router->get('/countries/{id}', array('as'=>'Get Country By Id', 'uses'=>'CountryController@GetCountryById'));
    $router->put('/countries/{id}', array('as'=>'Update Country', 'uses'=>'CountryController@UpdateCountry'));
    $router->delete('/countries/{id}', array('as'=>'Delete Country', 'uses'=>'CountryController@DeleteCountry'));

    /**     Locations */

    $router->post('/locations', array('as'=>'Create Location', 'uses'=>'LocationController@CreateLocation'));
    $router->get('/locations', array('as'=>'List Location', 'uses'=>'LocationController@GetAllLocations'));
    $router->get('/locations/{id}', array('as'=>'Location Detail', 'uses'=>'LocationController@GetLocationById'));
    $router->put('/locations/{id}', array('as'=>'Location Update', 'uses'=>'LocationController@UpdateLocation'));
    $router->delete('/locations/{id}', array('as'=>'Location delete', 'uses'=>'LocationController@DeleteLocation'));

    $router->post('/search/location', array('as'=>'Location search', 'uses'=>'LocationController@SearchLocationByName'));
    $router->get('/location/detail/{id}', array('as'=>'Location Detail', 'uses'=>'LocationController@GetLocationDetails'));

    //Buildings
    $router->get('/buildings', array('as'=>'Building List', 'uses'=>'BuildingController@Buildings'));
    $router->get('/buildings/{id}', array('as'=>'Building Details', 'uses'=>'BuildingController@GetBuildingDetail'));
    $router->get('/building/{id}/edit', array('as'=>'Building By Id', 'uses'=>'BuildingController@GetBuildingbyId'));
    $router->put('/buildings/{id}', array('as'=>'Building Update', 'uses'=>'BuildingController@UpdateBuilding'));
    $router->post('/buildings', array('as'=>'Create Building', 'uses'=>'BuildingController@CreateBuilding'));
    $router->delete('/buildings/{id}', array('as'=>'Delete Building', 'uses'=>'BuildingController@DeleteBuilding'));

    //Rooms
    $router->get('/rooms', array('as'=>'Building List', 'uses'=>'RoomController@rooms'));
    $router->get('/rooms/{id}', array('as'=>'Room Details', 'uses'=>'RoomController@getRoomDetail'));
    $router->get('/room/{id}/edit', array('as'=>'Room Edit', 'uses'=>'RoomController@getEditRoomDetail'));
    $router->put('/rooms/{id}', array('as'=>'Room Update', 'uses'=>'RoomController@updateRoom'));
    $router->post('/rooms', array('as'=>'Create Room', 'uses'=>'RoomController@createRoom'));
    $router->delete('/rooms/{id}', array('as'=>'Delete Room', 'uses'=>'RoomController@deleteRoom'));
});
