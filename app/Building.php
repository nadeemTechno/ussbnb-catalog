<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends Model
{
    use SoftDeletes;
    protected $table = 'buildings';
    protected $guarded = ['image'];

    public function locations()
    {
        return $this->hasOne('App\Location', 'id', 'location_id');
    }

    public function rooms() {
        return $this->belongsTo('App\Room');
    }

    public function image(){
        return $this->morphMany(Image::class, 'imageable');
    }
}
