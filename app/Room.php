<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;
    protected $table = 'rooms';
    protected $fillable = [
        'building_id',
        'name',
        'details',
        'price_per_hour',
        'maximum_person',
        'room_type',
        'working_hours_from',
        'working_hours_till',
        'status'
    ];

    public function buildings()
    {
        return $this->hasMany('App\Building', 'id', 'building_id');
    }

    public function image(){
        return $this->morphMany(Image::class, 'imageable');
    }
}
