<?php

namespace App\Http\Controllers;

use App\Image;
use App\Room;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use Exception;

class RoomController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }
    // get all rooms
    function rooms(){

        try {
            $rooms = Room::with('buildings')->get();

            if($rooms) {

                $data = [
                    'success' => true,
                    'message' => 'Rooms listing',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'rooms'=> $rooms
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_ACCEPTED);
            }
            return $this->errorResponse("No data available", Response::HTTP_NOT_ACCEPTABLE);

        } catch(Exception $e ) {
            return $this->errorResponse('Error, failed to get response', Response::HTTP_BAD_REQUEST);
        }

    }

    //insert new Room
    function createRoom(Request $request){
        Log::debug("buildings", array(0=>$request->all()));
        $rule = [
            'name' => 'required|string',
            'building_id' => 'required',
            'price_per_hour' => 'required',
            'maximum_person' => 'required',
            'room_type' => 'required',
            'image' => 'image:jpeg,png,gif'
        ];

        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation Fails', Response::HTTP_NOT_ACCEPTABLE);
        }

        try {

            $room = Room::create([
                'name' =>  $request->input('name'),
                'building_id' =>  $request->input('building_id'),
                'price_per_hour' => $request->input('price_per_hour'),
                'maximum_person' => $request->input('maximum_person'),
                'room_type' => $request->input('room_type'),
                'working_hours_from' => $request->input('working_hours_from'),
                'working_hours_till' => $request->input('working_hours_till'),
                'details' => $request->input('details'),
                'status' => 1,
            ]);

            if($room) {
                $data['message'] = "Room successfully created";
                $data['data'] = $room;
                $data = [
                    'success' => true,
                    'message' => 'Rooms created',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'rooms'=> $room
                    ]
                ];
                return $this->successResponse($data, Response::HTTP_CREATED);
            }
            return $this->errorResponse(
                "Error, Fail to create a room ", Response::HTTP_FORBIDDEN);

        } catch(Exception $e) {

            return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    //get a single room details
    function getRoomDetail($id){
        try{

            $room = Room::with('buildings', 'buildings.locations')->where('id', $id)->first();

            if($room) {

                $data = [
                    'success' => true,
                    'message' => 'Room',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'rooms'=> $room
                    ]
                ];
                return $this->successResponse($data, Response::HTTP_OK);
            }
            return $this->errorResponse(
                "Error, Fail to get room ", Response::HTTP_FORBIDDEN);
        } catch(Exception $e) {
            return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }


    

    //get a single room details
    function getEditRoomDetail($id){
        try {
            $room = Room::where('id', $id)->first();

            if($room) {
                $data = [
                    'success' => true,
                    'message' => 'Room',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'rooms'=> $room
                    ]
                ];
                   
                return $this->successResponse($data, Response::HTTP_ACCEPTED);
            }
            return $this->errorResponse("Error, Record is not available", Response::HTTP_NOT_ACCEPTABLE);

        } catch(Exception $e) {
            return $this->errorResponse("Unable to get the room record", Response::HTTP_BAD_REQUEST);
        }

    }

    /* Update Method */
    function updateRoom(Request $request){
        try {
            $rule = [
                'name' => 'required|string',
                'building_id' => 'required',
                'price_per_hour' => 'required',
                'maximum_person' => 'required',
                'room_type' => 'required',
                'status' => 'required'
            ];

            $validation = Validator::make($request->all(), $rule);
            if ($validation->fails()) {
                $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
                return $this->errorResponse('Validation Fails ', $return_arr);
            }

            $room = Room::find($request->get('id'));

            $room->building_id = $request->get('building_id');
            $room->name = $request->get('name');
            $room->details = $request->get('details');
            $room->price_per_hour = $request->get('price_per_hour');
            $room->maximum_person = $request->get('maximum_person');
            $room->room_type = $request->input('room_type');
            $room->working_hours_from = $request->input('working_hours_from');
            $room->working_hours_till = $request->input('working_hours_till');
            $room->status = $request->get('status');

            $room = $room->save();
            if($room) {
                $data = [
                    'success' => true,
                    'message' => 'Room updated',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'rooms'=> $room
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            }

            return $this->errorResponse("Failed to update the record", Response::HTTP_NOT_ACCEPTABLE);
        } catch(Exception $e) {
            return $this->errorResponse("Unable to update the record", Response::HTTP_BAD_REQUEST);
        }
    }

    function deleteRoom($id){
        try {
            $room = Room::find($id);

            if($room) {
                $room->delete();

                $data = [
                    'success' => true,
                    'message' => 'Room deleted',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'rooms'=> $room
                    ]
                ];
                return $this->successResponse($data, Response::HTTP_ACCEPTED);
            }
            return $this->errorResponse()("failed to delete the record", Response::HTTP_NOT_ACCEPTABLE);

        } catch(Exception $e) {
            return $this->errorResponse("Unable to delete the record", Response::HTTP_BAD_REQUEST);
        }
    }

    private function _storeImage($room) {

        try {
            if(request()->has('image')) {

                $Obj = new Image;
                $Obj->image_url = request()->image->store("uploads/rooms/$room->id", 'public');
                $Obj->imageable_id = $room->id;
                $Obj->imageable_type = 'App\Room';
                $Obj->save();
                return $Obj;
            }
        } catch(Exception $e) {
            return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}
